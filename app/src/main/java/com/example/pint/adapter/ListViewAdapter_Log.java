package com.example.pint.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filterable;
import android.widget.TextView;

import com.example.pint.LogExplorerActivity;
import  com.example.pint.R;
import  com.example.pint.database.entities.log.DBLog;
import com.example.pint.enums.SharedPreferencesEnum;
import com.example.pint.util.SharedPreferenceHelper;

import java.util.List;

public class ListViewAdapter_Log extends ArrayAdapter<DBLog> implements Filterable {

    String TAG = ListViewAdapter_Log.class.getName();

    private List<DBLog> items;

    public ListViewAdapter_Log(Context context, List<DBLog> log) {
        super(context, R.layout.listview_log_row, log);
        this.items=log;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.listview_log_row, null);
        }

        DBLog l = getItem(position);

        //Careful Spams the log fil if many Materialen were captured.
//        DBLog.e(TAG, "Trying to show all rows in adapter"+m.toString());

        TextView textView_datum = (TextView) v.findViewById(R.id.row_log_textview_datum);
        TextView textView_type = (TextView) v.findViewById(R.id.row_log_textview_type);
        TextView textView_user = (TextView) v.findViewById(R.id.row_log_textview_user);
        TextView textView_log = (TextView) v.findViewById(R.id.row_log_textview_log);

        textView_datum.setText(l.getDatum());
        textView_type.setText(l.getType());
        textView_user.setText(SharedPreferenceHelper.getSharedPreferenceString(getContext(), SharedPreferencesEnum.ACTIVE_USER.toString(), ""));
        textView_log.setText(l.getText());

        switch (l.getType()) {
            case "INFO":
                textView_datum.setTextColor(getContext().getResources().getColor(R.color.md_blue_600));
                textView_type.setTextColor(getContext().getResources().getColor(R.color.md_blue_600));
                textView_user.setTextColor(getContext().getResources().getColor(R.color.md_blue_600));
                textView_log.setTextColor(getContext().getResources().getColor(R.color.md_blue_600));
                break;
            case "DEBUG":
                textView_datum.setTextColor(getContext().getResources().getColor(R.color.md_purple_300));
                textView_type.setTextColor(getContext().getResources().getColor(R.color.md_purple_300));
                textView_user.setTextColor(getContext().getResources().getColor(R.color.md_purple_300));
                textView_log.setTextColor(getContext().getResources().getColor(R.color.md_purple_300));
                break;
            case "WARNING":
                textView_datum.setTextColor(getContext().getResources().getColor(R.color.md_orange_600));
                textView_type.setTextColor(getContext().getResources().getColor(R.color.md_orange_600));
                textView_user.setTextColor(getContext().getResources().getColor(R.color.md_orange_600));
                textView_log.setTextColor(getContext().getResources().getColor(R.color.md_orange_600));
                break;
            case "ERROR":
                textView_datum.setTextColor(getContext().getResources().getColor(R.color.md_red_600));
                textView_type.setTextColor(getContext().getResources().getColor(R.color.md_red_600));
                textView_user.setTextColor(getContext().getResources().getColor(R.color.md_red_600));
                textView_log.setTextColor(getContext().getResources().getColor(R.color.md_red_600));
                break;
            case "ARCHIVE":
                textView_datum.setTextColor(getContext().getResources().getColor(R.color.md_grey_600));
                textView_type.setTextColor(getContext().getResources().getColor(R.color.md_grey_600));
                textView_user.setTextColor(getContext().getResources().getColor(R.color.md_grey_600));
                textView_log.setTextColor(getContext().getResources().getColor(R.color.md_grey_600));
                break;
            default:
                textView_datum.setTextColor(getContext().getResources().getColor(R.color.md_black_1000));
                textView_type.setTextColor(getContext().getResources().getColor(R.color.md_black_1000));
                textView_user.setTextColor(getContext().getResources().getColor(R.color.md_black_1000));
                textView_log.setTextColor(getContext().getResources().getColor(R.color.md_black_1000));
                break;
        }

        return v;
    }

    public List<DBLog> getAllItems() {
        return items;
    }

    }

