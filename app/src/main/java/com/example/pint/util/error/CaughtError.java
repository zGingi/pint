package com.example.pint.util.error;


public class CaughtError {

    StringBuilder errorList;
    String errorMessage;
    String errorTitel;
    String errorSendButton;

    public CaughtError() {
    }

    public CaughtError(StringBuilder errorList, String errorMessage, String errorTitel, String errorSendButton) {
        this.errorList = errorList;
        this.errorMessage = errorMessage;
        this.errorTitel = errorTitel;
        this.errorSendButton = errorSendButton;
    }

    public StringBuilder getErrorList() {
        return errorList;
    }

    public void setErrorList(StringBuilder errorList) {
        this.errorList = errorList;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorTitel() {
        return errorTitel;
    }

    public void setErrorTitel(String errorTitel) {
        this.errorTitel = errorTitel;
    }

    public String getErrorSendButton() {
        return errorSendButton;
    }

    public void setErrorSendButton(String errorSendButton) {
        this.errorSendButton = errorSendButton;
    }

    @Override
    public String toString() {
        return "CaughtError{" +
                "errorList=" + errorList +
                ", errorMessage='" + errorMessage + '\'' +
                ", errorTitel='" + errorTitel + '\'' +
                ", errorSendButton='" + errorSendButton + '\'' +
                '}';
    }
}
