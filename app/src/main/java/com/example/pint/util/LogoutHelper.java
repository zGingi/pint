package com.example.pint.util;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;

import androidx.appcompat.app.AlertDialog;

import com.example.pint.LoginActivity;
import com.example.pint.R;
import com.example.pint.database.model.settings.SettingsModel;
import com.example.pint.enums.SharedPreferencesEnum;

import java.util.concurrent.ExecutionException;

public class LogoutHelper {


    public static void logout(final Activity activity, final SettingsModel settingsModel) {
        //Ask User to logout
        new AlertDialog.Builder(activity)
                .setTitle(activity.getResources().getString(R.string.abmelden_upper_case))
                .setMessage(activity.getResources().getString(R.string.wirklich_abmelden) + "?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(R.string.abmelden_upper_case, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {

                        try {

                            new AsyncTask<Void, Void, Void>() {
                                @Override
                                protected Void doInBackground(Void... params) {
                                    settingsModel.setActiveUser(null);
                                    SharedPreferenceHelper.deletePreference(activity, SharedPreferencesEnum.ACTIVE_USER_ID.toString());
                                    SharedPreferenceHelper.deletePreference(activity, SharedPreferencesEnum.ACTIVE_USER.toString());
                                    return null;
                                }

                                @Override
                                protected void onPostExecute(Void v) {
                                    Intent intent = new Intent(activity, LoginActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.putExtra("Source", "LogoutHelper");
                                    new DBLogWriter().info(activity, "User succesfully Logged out");
                                    activity.startActivity(intent);
                                }
                            }.execute().get();
                        } catch (InterruptedException | ExecutionException e) {
                           new ErrorHelper().showErrorAlert_Exception(activity, e); //e.printStackTrace();
                        }

                    }
                }).setNegativeButton(R.string.abbrechen, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        }).show();
    }
}
