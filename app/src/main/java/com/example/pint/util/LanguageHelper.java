package com.example.pint.util;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;

import com.example.pint.enums.SharedPreferencesEnum;

import java.util.Locale;

public class LanguageHelper {
    /**
     * Update the app language
     *
     * Source: https://stackoverflow.com/questions/46485206/language-set-to-default-after-app-close-in-android
     *
     * @param context Context
     */
    public static void updateLanguage(final Context context) {

        //Get Active Language
        String languageToLoad = "de";
        switch (SharedPreferenceHelper.getSharedPreferenceInt(context, SharedPreferencesEnum.ACTIVE_LANGUAGE.toString(), 1)) {
            case 1:
                languageToLoad = "de";
                break;
            case 2:
                languageToLoad = "fr";
                break;
            case 3:
                languageToLoad = "it";
                break;
        }

        DBLogWriter dbLogWriter = new DBLogWriter();

        dbLogWriter.info(context, "Active Language: " + languageToLoad);

        final Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);

        Resources res = context.getResources();
        Configuration cfg = new Configuration(res.getConfiguration());
        cfg.locale = locale;
        res.updateConfiguration(cfg, res.getDisplayMetrics());

    }

}