package com.example.pint.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.StatFs;

import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;

import com.example.pint.database.entities.settings.Settings;
import com.example.pint.database.model.settings.SettingsModel;
import com.example.pint.enums.ErrorCodesEnum;
import com.example.pint.enums.SharedPreferencesEnum;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ExecutionException;


//Source:
//https://stackoverflow.com/questions/18010309/android-catch-error-unfortunately-app-has-stopped-working
public class UnCaughtException implements Thread.UncaughtExceptionHandler {

    private Context context;
    SettingsModel settingsModel;
    DBLogWriter dbLogWriter;

    public UnCaughtException(Context ctx) {

        context = ctx;
        settingsModel = ViewModelProviders.of((FragmentActivity) context).get(SettingsModel.class);
        dbLogWriter = new DBLogWriter();

    }

    private StatFs getStatFs() {
        File path = Environment.getDataDirectory();
        return new StatFs(path.getPath());
    }

    private long getAvailableInternalMemorySize(StatFs stat) {
        long blockSize = stat.getBlockSize();
        long availableBlocks = stat.getAvailableBlocks();
        return availableBlocks * blockSize;
    }

    private long getTotalInternalMemorySize(StatFs stat) {
        long blockSize = stat.getBlockSize();
        long totalBlocks = stat.getBlockCount();
        return totalBlocks * blockSize;
    }

    private void addInformation(StringBuilder message) {
        message.append("Locale: ").append(Locale.getDefault()).append('\n');
        try {
            PackageManager pm = context.getPackageManager();
            PackageInfo pi;
            pi = pm.getPackageInfo(context.getPackageName(), 0);
            message.append("Version: ").append(pi.versionName).append('\n');
            message.append("Package: ").append(pi.packageName).append('\n');
        } catch (Exception e) {
            dbLogWriter.error(context, e,"Caught Exception: " + Arrays.toString(e.getStackTrace()), ErrorCodesEnum.ERROR_00002, false);
            message.append("Could not get Version information for ").append(
                    context.getPackageName());
        }
        message.append("Phone Model ").append(android.os.Build.MODEL)
                .append('\n');
        message.append("Android Version : ")
                .append(android.os.Build.VERSION.RELEASE).append('\n');
        message.append("Board: ").append(android.os.Build.BOARD).append('\n');
        message.append("Brand: ").append(android.os.Build.BRAND).append('\n');
        message.append("Device: ").append(android.os.Build.DEVICE).append('\n');
        message.append("Host: ").append(android.os.Build.HOST).append('\n');
        message.append("ID: ").append(android.os.Build.ID).append('\n');
        message.append("Model: ").append(android.os.Build.MODEL).append('\n');
        message.append("Product: ").append(android.os.Build.PRODUCT)
                .append('\n');
        message.append("Type: ").append(android.os.Build.TYPE).append('\n');
        StatFs stat = getStatFs();
        message.append("Total Internal memory: ")
                .append(getTotalInternalMemorySize(stat)).append('\n');
        message.append("Available Internal memory: ")
                .append(getAvailableInternalMemorySize(stat)).append("\n\n");
    }

    private void addSettings(final StringBuilder message) {

        try {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {

                    Settings settings = settingsModel.getAllSettings();

                    if (null != settings) {

                        String activeUser = "";
                        if (null != settings.getActive_user()) {
                            activeUser = settings.getActive_user();
                        }

                        int activeEnviroment = 0;
                        if (null != settings.getEnviroment()) {
                            activeEnviroment = settings.getEnviroment();
                        }

                        message.append("Active User: ").append(activeUser).append('\n');
                        message.append("Active Enviroment: ").append(activeEnviroment).append('\n').append('\n');

                    }else{
                        message.append("Settings: Empty").append('\n');
                    }

                    return null;
                }
            }.execute().get();
        } catch (InterruptedException | ExecutionException e) {
            dbLogWriter.error(context, e,"InterruptedException | ExecutionException:" + Arrays.toString(e.getStackTrace()), ErrorCodesEnum.ERROR_00002, true);
        }

    }

    public void uncaughtException(Thread t, Throwable e) {
        try {
            StringBuilder report = new StringBuilder();
            Date curDate = new Date();
            report.append("CaughtError Report collected on : ")
                    .append(curDate).append('\n').append('\n');
            report.append("Informations :").append('\n');
            addInformation(report);
            report.append('\n').append('\n');
            report.append("Settings :").append('\n');
            addSettings(report);
            report.append('\n').append('\n');
            report.append("Stack:\n");
            final Writer result = new StringWriter();
            final PrintWriter printWriter = new PrintWriter(result);
            e.printStackTrace(printWriter);
            report.append(result.toString());
            printWriter.close();
            report.append('\n');
            report.append("**** End of current Report ***");


            //Save error to Shared Preferences to display after restart of app
            SharedPreferenceHelper.setSharedPreferenceInt(context, SharedPreferencesEnum.UNCAUGHT_EXCEPTION_OCCURRED.toString(), 1);
            SharedPreferenceHelper.setSharedPreferenceString(context, SharedPreferencesEnum.UNCAUGHT_EXCEPTION_CONTENT.toString(), report.toString());

            dbLogWriter.error(context, null, report.toString(), ErrorCodesEnum.ERROR_00002, false, true);

        } catch (Throwable ignore) {
            dbLogWriter.error(context, null, "CaughtError while sending error e-mail: " + ignore , ErrorCodesEnum.ERROR_00002, true);
        }
    }
}