package com.example.pint.util;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;

import com.example.pint.database.entities.log.DBLog;
import com.example.pint.database.model.log.DBLogModel;
import com.example.pint.enums.ErrorCodesEnum;
import com.example.pint.enums.LogENUM;
import com.example.pint.enums.SharedPreferencesEnum;


public class DBLogWriter {

    private static final GetFormattedDate getFormattedDate = new GetFormattedDate();
    DBLogModel dbLogModel;

    String activeUser;
    int activeProfile;
    int activeAppProfil;
    int activeEnviroment;
    int isDebug;
    String fullClassName;
    String className;
    String methodName;
    int lineNumber;

    String logText;

    public void info(final Context ctx, final String text) {

        //CR60 - v1.0.6
        //Added Additional Content to Log File
        //Soruce: https://stackoverflow.com/questions/115008/how-can-we-print-line-numbers-to-the-log-in-java
        fullClassName = Thread.currentThread().getStackTrace()[3].getClassName();
        className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
        methodName = Thread.currentThread().getStackTrace()[3].getMethodName();
        lineNumber = Thread.currentThread().getStackTrace()[3].getLineNumber();

        //@INFO
        //Get Data from logged in user
//        activeUser = SharedPreferenceHelper.getSharedPreferenceString((FragmentActivity) ctx, SharedPreferencesEnum.ACTIVE_USER_ID.toString(), "SYSTEM");
        activeEnviroment = SharedPreferenceHelper.getSharedPreferenceInt(ctx, SharedPreferencesEnum.ACTIVE_ENVIROMENT.toString(), 0);
        isDebug = SharedPreferenceHelper.getSharedPreferenceInt(ctx, SharedPreferencesEnum.IS_DEBUG_ENABLED.toString(), 0);

        //logText = fullClassName+"\n"+className+ " - "+ methodName +"("+lineNumber+"):\t"+text
        logText = "[A" + activeAppProfil + "|P" + activeProfile + "|E" + activeEnviroment + "|D" + isDebug + "] " + className + " - " + methodName + "(" + lineNumber + "):\t" + text;

        final DBLog log = new DBLog(LogENUM.INFO.toString(), logText, getFormattedDate.GetFormattedDateForLogEntry(), SharedPreferenceHelper.getSharedPreferenceString(ctx, SharedPreferencesEnum.ACTIVE_USER.toString(), "SYSTEM"));

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected void onPreExecute() {
                dbLogModel = ViewModelProviders.of((FragmentActivity) ctx).get(DBLogModel.class);
            }

            @Override
            protected Void doInBackground(Void... params) {
                dbLogModel.addDBLog(log);
                return null;
            }
        }.execute();

        Log.i("DBLogWriter", logText);

    }

    public void archive(final Context ctx, final String text) {

        fullClassName = Thread.currentThread().getStackTrace()[3].getClassName();
        className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
        methodName = Thread.currentThread().getStackTrace()[3].getMethodName();
        lineNumber = Thread.currentThread().getStackTrace()[3].getLineNumber();

        //@INFO
        //Get Data from logged in user
//        activeUser = SharedPreferenceHelper.getSharedPreferenceString((FragmentActivity) ctx, SharedPreferencesEnum.ACTIVE_USER_ID.toString(), "SYSTEM");
        activeEnviroment = SharedPreferenceHelper.getSharedPreferenceInt(ctx, SharedPreferencesEnum.ACTIVE_ENVIROMENT.toString(), 0);
        isDebug = SharedPreferenceHelper.getSharedPreferenceInt(ctx, SharedPreferencesEnum.IS_DEBUG_ENABLED.toString(), 0);

        //logText = fullClassName+"\n"+className+ " - "+ methodName +"("+lineNumber+"):\t"+text
        logText = "[A" + activeAppProfil + "|P" + activeProfile + "|E" + activeEnviroment + "|D" + isDebug + "] " + className + " - " + methodName + "(" + lineNumber + "):\t" + text;

        final DBLog log = new DBLog(LogENUM.ARCHIVE.toString(), logText, getFormattedDate.GetFormattedDateForLogEntry(), SharedPreferenceHelper.getSharedPreferenceString(ctx, SharedPreferencesEnum.ACTIVE_USER.toString(), "SYSTEM"));


        new AsyncTask<Void, Void, Void>() {

            @Override
            protected void onPreExecute() {
                dbLogModel = ViewModelProviders.of((FragmentActivity) ctx).get(DBLogModel.class);
            }

            @Override
            protected Void doInBackground(Void... params) {
                dbLogModel.addDBLog(log);
                return null;
            }
        }.execute();

        Log.d("DBLogWriter", logText);

    }

    public void debug(final Context ctx, final String text) {

        fullClassName = Thread.currentThread().getStackTrace()[3].getClassName();
        className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
        methodName = Thread.currentThread().getStackTrace()[3].getMethodName();
        lineNumber = Thread.currentThread().getStackTrace()[3].getLineNumber();

        //@INFO
        //Get Data from logged in user
//        activeUser = SharedPreferenceHelper.getSharedPreferenceString((FragmentActivity) ctx, SharedPreferencesEnum.ACTIVE_USER_ID.toString(), "SYSTEM");
        activeEnviroment = SharedPreferenceHelper.getSharedPreferenceInt(ctx, SharedPreferencesEnum.ACTIVE_ENVIROMENT.toString(), 0);
        isDebug = SharedPreferenceHelper.getSharedPreferenceInt(ctx, SharedPreferencesEnum.IS_DEBUG_ENABLED.toString(), 0);

        //logText = fullClassName+"\n"+className+ " - "+ methodName +"("+lineNumber+"):\t"+text
        logText = "[A" + activeAppProfil + "|P" + activeProfile + "|E" + activeEnviroment + "|D" + isDebug + "] " + className + " - " + methodName + "(" + lineNumber + "):\t" + text;

        //@INFO
        //Dont store DEBUG Log in the DB if Debug modus is not active
        if (SharedPreferenceHelper.getSharedPreferenceInt(ctx, SharedPreferencesEnum.IS_DEBUG_ENABLED.toString(), 0) == 1) {
            //START DEBUG****
            final DBLog log = new DBLog(LogENUM.DEBUG.toString(), logText, getFormattedDate.GetFormattedDateForLogEntry(), SharedPreferenceHelper.getSharedPreferenceString(ctx, SharedPreferencesEnum.ACTIVE_USER.toString(), "SYSTEM"));

            new AsyncTask<Void, Void, Void>() {

                @Override
                protected void onPreExecute() {
                    dbLogModel = ViewModelProviders.of((FragmentActivity) ctx).get(DBLogModel.class);
                }

                @Override
                protected Void doInBackground(Void... params) {
                    dbLogModel.addDBLog(log);
                    return null;
                }
            }.execute();
            //END DEBUG****
        }

        Log.d("DBLogWriter", logText);

    }

    public void warning(final Context ctx, final String text) {

        fullClassName = Thread.currentThread().getStackTrace()[3].getClassName();
        className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
        methodName = Thread.currentThread().getStackTrace()[3].getMethodName();
        lineNumber = Thread.currentThread().getStackTrace()[3].getLineNumber();

        //@INFO
        //Get Data from logged in user
//        activeUser = SharedPreferenceHelper.getSharedPreferenceString((FragmentActivity) ctx, SharedPreferencesEnum.ACTIVE_USER_ID.toString(), "SYSTEM");
        activeEnviroment = SharedPreferenceHelper.getSharedPreferenceInt(ctx, SharedPreferencesEnum.ACTIVE_ENVIROMENT.toString(), 0);
        isDebug = SharedPreferenceHelper.getSharedPreferenceInt(ctx, SharedPreferencesEnum.IS_DEBUG_ENABLED.toString(), 0);

        //logText = fullClassName+"\n"+className+ " - "+ methodName +"("+lineNumber+"):\t"+text
        logText = "[A" + activeAppProfil + "|P" + activeProfile + "|E" + activeEnviroment + "|D" + isDebug + "] " + className + " - " + methodName + "(" + lineNumber + "):\t" + text;

        final DBLog log = new DBLog(LogENUM.WARNING.toString(), logText, getFormattedDate.GetFormattedDateForLogEntry(), SharedPreferenceHelper.getSharedPreferenceString(ctx, SharedPreferencesEnum.ACTIVE_USER.toString(), "SYSTEM"));

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected void onPreExecute() {
                dbLogModel = ViewModelProviders.of((FragmentActivity) ctx).get(DBLogModel.class);
            }

            @Override
            protected Void doInBackground(Void... params) {
                dbLogModel.addDBLog(log);
                return null;
            }
        }.execute();

        Log.w("DBLogWriter", logText);

    }

    public void error(final Context ctx, final Exception e, final String text, final ErrorCodesEnum errorCodesEnum, final boolean displayErrorWindow) {
        error(ctx, e, text, errorCodesEnum, displayErrorWindow, true);
    }

    public void error(final Context ctx, final Exception e, final String text, final ErrorCodesEnum errorCodesEnum, final boolean displayErrorWindow, final boolean restartApp) {

        fullClassName = Thread.currentThread().getStackTrace()[3].getClassName();
        className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
        methodName = Thread.currentThread().getStackTrace()[3].getMethodName();
        lineNumber = Thread.currentThread().getStackTrace()[3].getLineNumber();

        //@INFO
        //Get Data from logged in user
//        activeUser = SharedPreferenceHelper.getSharedPreferenceString((FragmentActivity) ctx, SharedPreferencesEnum.ACTIVE_USER_ID.toString(), "SYSTEM");
        activeEnviroment = SharedPreferenceHelper.getSharedPreferenceInt(ctx, SharedPreferencesEnum.ACTIVE_ENVIROMENT.toString(), 0);
        isDebug = SharedPreferenceHelper.getSharedPreferenceInt(ctx, SharedPreferencesEnum.IS_DEBUG_ENABLED.toString(), 0);

        //logText = fullClassName+"\n"+className+ " - "+ methodName +"("+lineNumber+"):\t"+text
        logText = "[A" + activeAppProfil + "|P" + activeProfile + "|E" + activeEnviroment + "|D" + isDebug + "] " + className + " - " + methodName + "(" + lineNumber + "):\t \"" + errorCodesEnum.getEnumString() + "\" - " + text;

        final DBLog log = new DBLog(LogENUM.ERROR.toString(), logText, getFormattedDate.GetFormattedDateForLogEntry(), SharedPreferenceHelper.getSharedPreferenceString(ctx, SharedPreferencesEnum.ACTIVE_USER.toString(), "SYSTEM"));

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected void onPreExecute() {
                dbLogModel = ViewModelProviders.of((FragmentActivity) ctx).get(DBLogModel.class);
            }

            @Override
            protected Void doInBackground(Void... params) {
                dbLogModel.addDBLog(log);
                return null;
            }
        }.execute();

        Log.e("DBLogWriter", logText);

        // Display Error window if displayErrorWindow is true
        new ErrorHelper().showErrorAlert_Exception(ctx, e, errorCodesEnum, logText, displayErrorWindow, restartApp);

        //If isDebug = true --> print Stacktrace of error
        if (isDebug == 1) {
            e.printStackTrace();
        }
    }

    public void summary(final Context ctx, final String text) {

        fullClassName = Thread.currentThread().getStackTrace()[3].getClassName();
        className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
        methodName = Thread.currentThread().getStackTrace()[3].getMethodName();
        lineNumber = Thread.currentThread().getStackTrace()[3].getLineNumber();

        //@INFO
        //Get Data from logged in user
        activeEnviroment = SharedPreferenceHelper.getSharedPreferenceInt(ctx, SharedPreferencesEnum.ACTIVE_ENVIROMENT.toString(), 0);
        isDebug = SharedPreferenceHelper.getSharedPreferenceInt(ctx, SharedPreferencesEnum.IS_DEBUG_ENABLED.toString(), 0);

        //logText = fullClassName+"\n"+className+ " - "+ methodName +"("+lineNumber+"):\t"+text
        logText = "[A" + activeAppProfil + "|P" + activeProfile + "|E" + activeEnviroment + "|D" + isDebug + "] " + className + " - " + methodName + "(" + lineNumber + "):\t" + text;

        final DBLog log = new DBLog(LogENUM.SUMMARY.toString(), logText, getFormattedDate.GetFormattedDateForLogEntry(), SharedPreferenceHelper.getSharedPreferenceString(ctx, SharedPreferencesEnum.ACTIVE_USER.toString(), "SYSTEM"));

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected void onPreExecute() {
                dbLogModel = ViewModelProviders.of((FragmentActivity) ctx).get(DBLogModel.class);
            }

            @Override
            protected Void doInBackground(Void... params) {
                dbLogModel.addDBLog(log);
                return null;
            }
        }.execute();

        Log.i("DBLogWriter", "[SUMMARY] " + logText);

    }

}
