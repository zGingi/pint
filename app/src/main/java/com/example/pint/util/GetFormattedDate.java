package com.example.pint.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class GetFormattedDate {

    DateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.GERMAN);
    DateFormat df2 = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss.SSS", Locale.GERMAN);
    DateFormat df_log = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.GERMAN);
    DateFormat df_archive = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.GERMAN);

    public String GetFormattedDate(){
        Date d = new Date();
        return df2.format(d);
    }

    public String GetFormattedDateForLogEntry(){
        Date d = new Date();
        return df_log.format(d);
    }

    public String GetFormattedDateTime(){
        Date d = new Date();
        return df.format(d);
    }

    public String GetFormattedDate(Date d){
        return df.format(d);
    }

    public String GetFormattedDateForArchive(String sdate){

        try {
            return df_archive.format(df.parse(sdate));
        } catch (ParseException e) {
           e.printStackTrace();
        }

        Date d = new Date();

        return df_archive.format(d);
    }

    public String GetFormattedDate(Date d, DateFormat df){
        return df.format(d);
    }

    public String parseDate(String sdate, DateFormat dateFormatFromString, DateFormat desiredDateFormat){
        try {
            return desiredDateFormat.format(dateFormatFromString.parse(sdate));
        } catch (ParseException e) {
           e.printStackTrace();
        }
        return sdate;
    }
}
