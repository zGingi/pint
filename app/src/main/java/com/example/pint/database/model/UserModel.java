package com.example.pint.database.model;

import android.app.Application;
import android.content.Context;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.pint.database.AppDatabase;
import com.example.pint.database.entities.User;
import com.example.pint.util.DBLogWriter;
import com.example.pint.util.GetFormattedDate;

import java.util.List;


public class UserModel extends AndroidViewModel {

    private static final String TAG = UserModel.class.getName();

    private AppDatabase mDb;

    public LiveData<List<User>>  activeUser;
    public List<User> userList;
    public User getUserWithCode;

    GetFormattedDate getFormattedDate;
    DBLogWriter dbLogWriter;

    public UserModel(Application application) {
        super(application);
        createDb();
        getFormattedDate = new GetFormattedDate();
        dbLogWriter = new DBLogWriter();

    }

    public void createDb() {

        mDb = AppDatabase.getAppDatabase(this.getApplication());

    }

    public void addUser(User user, Context context) {

        //@INFO
        dbLogWriter.info(context, "Add User: "+ user.toString());

        mDb.userDAO().insertUser(user);
    }

    public void deleteUser(User user, Context context){
        //@INFO
        dbLogWriter.info(context, "Delete User: "+ user.toString());

        mDb.userDAO().deleteUser(user);
    }

    public void deleteAllUser(Context context){

        List<User> userlist = getAllUser();

        for (User user : userlist){
            //@INFO
            dbLogWriter.info(context, "Delete User: "+ user.toString());

            mDb.userDAO().deleteUser(user);
        }


    }

    public void updateUser(User user, Context context) {

        //@INFO
        dbLogWriter.info(context, "Update User: "+ user.toString());

        mDb.userDAO().updateUser(user);
    }

    public User getUserWithCode(String user_code) {

        getUserWithCode = mDb.userDAO().findUserWithCode(user_code);
        return getUserWithCode;
    }

    public List<User> getAllUser() {

        userList = mDb.userDAO().getAllUser();
        return userList;
    }

}