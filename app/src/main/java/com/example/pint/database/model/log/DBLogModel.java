package com.example.pint.database.model.log;

import android.app.Application;
import android.content.Context;

import androidx.lifecycle.AndroidViewModel;

import com.example.pint.database.AppDatabase;
import com.example.pint.database.entities.log.DBLog;
import com.example.pint.enums.SettingsEnum;
import com.example.pint.util.DBLogWriter;

import java.util.List;


public class DBLogModel extends AndroidViewModel {

    private static final String TAG = DBLogModel.class.getName();

    private AppDatabase mDb;

    public List<DBLog> activeDBLogList;
    public DBLog getDBLogWithCode;

    public DBLogModel(Application application) {
        super(application);
        createDb();

    }

    public void createDb() {

        mDb = AppDatabase.getAppDatabase(this.getApplication());


    }

    public List<DBLog> getAllDBLog(boolean restricted, int limit) {
        if(restricted){
            return mDb.dbLogDAO().getAllDBLog_Restricted(limit);
        }else{
            return mDb.dbLogDAO().getAllDBLog();
        }
    }


    public void addDBLog(DBLog log) {
        //On Creation, set Log Uploaded to 0 = Not Uploaded
        log.setUploaded(0);
        mDb.dbLogDAO().insertDBLog(log);
    }

    public void updateDBLog(DBLog log) {
        mDb.dbLogDAO().updateDBLog(log);
    }

    public void deleteDBLog(DBLog log){
        mDb.dbLogDAO().deleteDBLog(log);
    }

    public void deleteAllLogsFromDate(String datum){
        mDb.dbLogDAO().deleteAllLogsFromDate(datum);
    }

    public List<String> getLogDistinctDates(){
        return mDb.dbLogDAO().getLogDistinctDates();
    }

    public List<DBLog> getLogsForDate(String datum, int uploaded){
        if(uploaded == 0){
            return mDb.dbLogDAO().getLogsForDate(datum);
        }else{
            return mDb.dbLogDAO().getLogsForDate(datum, 1);
        }
    }

    public int countLogsForDate(String datum){
        return mDb.dbLogDAO().countLogsForDate(datum);
    }

    public int countLogsWithTypeForDate(String datum, String type){
        return mDb.dbLogDAO().countLogsWithTypeForDate(datum, type);
    }

    public void deleteOldLogs(Context ctx){

        DBLogWriter dbLogWriter = new DBLogWriter();

        List<DBLog> oldLogsToDelete;

        switch (SettingsEnum.ARCHIVE_LIFE_IN_DAYS.getEnumInt()){
            case 30:
                oldLogsToDelete = mDb.dbLogDAO().getOldLogsToDelete30();

                if(oldLogsToDelete.size() >= 0){
                    dbLogWriter.info(ctx, "Delete old Log (30). Total Entries deleted: "+oldLogsToDelete.size());
                    mDb.dbLogDAO().deleteOldLogs30();
                }else{
                    dbLogWriter.debug(ctx, "No old Log to delete");
                }
                break;

            case 60:
                oldLogsToDelete = mDb.dbLogDAO().getOldLogsToDelete60();

                if(oldLogsToDelete.size() >= 0){
                    dbLogWriter.info(ctx, "Delete old Log (60). Total Entries deleted: "+oldLogsToDelete.size());
                     mDb.dbLogDAO().deleteOldLogs60();
                }else{
                    dbLogWriter.debug(ctx, "No old Log to delete");
                }
                break;

            case 90:
                oldLogsToDelete = mDb.dbLogDAO().getOldLogsToDelete90();

                if(oldLogsToDelete.size() >= 0){
                    dbLogWriter.info(ctx, "Delete old Log (90). Total Entries deleted: "+oldLogsToDelete.size());
                     mDb.dbLogDAO().deleteOldLogs90();
                }else{
                    dbLogWriter.debug(ctx, "No old Log to delete");
                }
                break;

            default:
                oldLogsToDelete = mDb.dbLogDAO().getOldLogsToDelete30();

                if(oldLogsToDelete.size() >= 0){
                    dbLogWriter.info(ctx, "Delete old Log (Default = 30). Total Entries deleted: "+oldLogsToDelete.size());
                     mDb.dbLogDAO().deleteOldLogs30();
                }else{
                    dbLogWriter.debug(ctx, "No old Log to delete");
                }
                break;
        }
    }

    public List<DBLog> getAllNotUploadedLogs(){
        return mDb.dbLogDAO().getAllNotUploadedLogs();
    }


    public void closeDB(){
        AppDatabase.destroyInstance();
    }




}