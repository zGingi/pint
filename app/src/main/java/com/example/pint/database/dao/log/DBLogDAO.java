package com.example.pint.database.dao.log;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.pint.database.entities.log.DBLog;

import java.util.List;

@Dao
public interface DBLogDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertDBLog(DBLog... DBLog);

    @Query("SELECT * FROM dblog ORDER BY id DESC LIMIT :limit")
    List<DBLog> getAllDBLog_Restricted(int limit);

    @Query("SELECT * FROM dblog ORDER BY id DESC")
    List<DBLog> getAllDBLog();

    @Query("SELECT DISTINCT substr(datum,1,10) FROM dblog ORDER BY substr(datum,1,10) DESC")
    List<String> getLogDistinctDates();

    @Query("SELECT * FROM dblog WHERE substr(datum,1,10) = :datum ORDER BY datum DESC")
    List<DBLog> getLogsForDate(String datum);

    @Query("SELECT * FROM dblog WHERE substr(datum,1,10) = :datum AND uploaded = :uploaded ORDER BY datum DESC")
    List<DBLog> getLogsForDate(String datum, int uploaded);

    @Query("SELECT COUNT(id) FROM dblog WHERE substr(datum,1,10) = :datum")
    int countLogsForDate(String datum);

    @Query("SELECT COUNT(id) FROM dblog WHERE substr(datum,1,10) = :datum AND type like :type")
    int countLogsWithTypeForDate(String datum, String type);

    @Update
    void updateDBLog(DBLog... DBLog);

    @Delete
    void deleteDBLog(DBLog... DBLog);

    @Query("DELETE FROM dblog WHERE substr(datum,1,10) = :datum")
    void deleteAllLogsFromDate(String datum);

    @Query("DELETE FROM dblog WHERE datum <= date('now','-30 day')")
    void deleteOldLogs30();

    @Query("DELETE FROM dblog WHERE datum <= date('now','-60 day')")
    void deleteOldLogs60();

    @Query("DELETE FROM dblog WHERE datum <= date('now','-90 day')")
    void deleteOldLogs90();

    @Query("SELECT * FROM dblog WHERE datum <= date('now','-30 day')")
    List<DBLog> getOldLogsToDelete30();

    @Query("SELECT * FROM dblog WHERE datum <= date('now','-60 day')")
    List<DBLog> getOldLogsToDelete60();

    @Query("SELECT * FROM dblog WHERE datum <= date('now','-90 day')")
    List<DBLog> getOldLogsToDelete90();

    @Query("SELECT * FROM dblog WHERE uploaded = 0 ORDER BY datum DESC")
    List<DBLog> getAllNotUploadedLogs();

}
