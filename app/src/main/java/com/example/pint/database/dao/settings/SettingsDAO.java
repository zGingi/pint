package com.example.pint.database.dao.settings;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.pint.database.entities.settings.Settings;

@Dao
public interface SettingsDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertSettings(Settings... leistung);

    @Query("SELECT * FROM settings WHERE id = (SELECT MAX(id) FROM settings)")
    Settings getAllSettings();

    @Update
    void updateSettings(Settings... setting);

    @Delete
    void deleteSettings(Settings... leistung);

    @Query("SELECT active_user FROM settings WHERE id = (SELECT MAX(id) FROM settings)")
    String getActiveUser();

    @Query("UPDATE settings SET " +
            "active_user = :activeUser " +
            "WHERE id = (SELECT MAX(id) FROM settings)")
    void setActiveUser(String activeUser);

    @Query("SELECT is_debug_modus_active FROM settings WHERE id = (SELECT MAX(id) FROM settings)")
    Integer getIsDebugModusActive();

    @Query("UPDATE settings SET " +
            "is_debug_modus_active = :isDebugModusActive " +
            "WHERE id = (SELECT MAX(id) FROM settings)")
    void setIsDebugModusActive(int isDebugModusActive);

    @Query("SELECT enviroment FROM settings WHERE id = (SELECT MAX(id) FROM settings)")
    Integer getEnviroment();

    @Query("UPDATE settings SET " +
            "enviroment = :enviroment " +
            "WHERE id = (SELECT MAX(id) FROM settings)")
    void setEnviroment(int enviroment);

    @Query("DELETE FROM settings")
    void deleteAllSettings();

}
