package com.example.pint.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.pint.database.dao.UserDAO;
import com.example.pint.database.dao.log.DBLogDAO;
import com.example.pint.database.dao.settings.SettingsDAO;
import com.example.pint.database.entities.User;
import com.example.pint.database.entities.log.DBLog;
import com.example.pint.database.entities.settings.Settings;

@Database(entities = {User.class, DBLog.class, Settings.class}
                        ,version = 1, exportSchema = false)

public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;

    public abstract UserDAO userDAO();
    public abstract DBLogDAO dbLogDAO();
    public abstract SettingsDAO settingsDAO();

    private static String DATABASE_NAME = "pint-database";

    //https://developer.android.com/reference/android/arch/persistence/room/RoomDatabase.html
    public static synchronized AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, DATABASE_NAME)
                            // To simplify the codelab, allow queries on the main thread.
                            // Don't do this on a real app! See PersistenceBasicSample for an example.
                            //.allowMainThreadQueries()
                            //.addMigrations(MIGRATION_1_2)
                            .build();
        }
        return INSTANCE;
    }

    //*****************************
    // Start Migration Scripts
    // Documentation: https://developer.android.com/topic/libraries/architecture/room.html#db-migration
    //*****************************

//    static final Migration MIGRATION_1_2 = new Migration(1, 2) {
//        @Override
//        public void migrate(SupportSQLiteDatabase database) {
//            database.execSQL("ALTER TABLE dblog "
//                    + " ADD COLUMN uploaded INTEGER");
//        }
//    };



    public static void deleteDB(Context context){
        INSTANCE.close();
        context.deleteDatabase(DATABASE_NAME);
        INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, DATABASE_NAME).build();

    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}