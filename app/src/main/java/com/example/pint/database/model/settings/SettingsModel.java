package com.example.pint.database.model.settings;

import android.app.Application;
import android.content.Context;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.pint.database.AppDatabase;
import com.example.pint.database.entities.settings.Settings;
import com.example.pint.enums.SharedPreferencesEnum;
import com.example.pint.util.DBLogWriter;
import com.example.pint.util.GetFormattedDate;
import com.example.pint.util.SharedPreferenceHelper;

import java.util.List;

public class SettingsModel extends AndroidViewModel {

    private static final String TAG = SettingsModel.class.getName();

    private AppDatabase mDb;

    public LiveData<Settings> activeSettings;
    public Settings getSettingsWithCode;
    public String getSingleSetting;
    public Integer getSingleSetting_int;
    public LiveData<List<String>> getMultipleSetting;

    GetFormattedDate getFormattedDate;
    DBLogWriter dbLogWriter;

    public SettingsModel(Application application) {
        super(application);
        createDb();
        getFormattedDate = new GetFormattedDate();

        dbLogWriter = new DBLogWriter();
    }

    public void createDb() {

        mDb = AppDatabase.getAppDatabase(this.getApplication());

    }

    public Settings getAllSettings() {

        getSettingsWithCode = mDb.settingsDAO().getAllSettings();
        return getSettingsWithCode;
    }

    public void addSettings(Settings setting, Context context) {

        dbLogWriter.info(context, "Add Settings: " + setting.toString());

        mDb.settingsDAO().insertSettings(setting);
    }

    public void updateSettings(Settings setting, Context context) {

        dbLogWriter.info(context, "Update Settings: " + setting.toString());

        mDb.settingsDAO().updateSettings(setting);
    }

    public String getActiveUser() {
        getSingleSetting = mDb.settingsDAO().getActiveUser();
        return getSingleSetting;
    }

    public int getIsDebugModusActive() {
        getSingleSetting_int = mDb.settingsDAO().getIsDebugModusActive();
        return (getSingleSetting_int != null ) ? getSingleSetting_int : 0;
    }

    public int getEnviroment() {
        getSingleSetting_int = mDb.settingsDAO().getEnviroment();
        return getSingleSetting_int;
    }

     /*
        *  **************************
        *   Setters
        *  **************************
        */

    public void setActiveUser(String activeUser) {
        mDb.settingsDAO().setActiveUser(activeUser);
    }

    public void setIsDebugModusActive(int isDebugModusActive, Context context) {

        dbLogWriter.info(context, "Set isDebugModus: " + isDebugModusActive);

        SharedPreferenceHelper.setSharedPreferenceInt(context, SharedPreferencesEnum.IS_DEBUG_ENABLED.toString(), isDebugModusActive);

        mDb.settingsDAO().setIsDebugModusActive(isDebugModusActive);
    }

    public void setEnviroment(int enviroment, Context context) {

        dbLogWriter.info(context, "Set Enviroment: " + enviroment);

        SharedPreferenceHelper.setSharedPreferenceInt(context, SharedPreferencesEnum.ACTIVE_ENVIROMENT.toString(), enviroment);

        mDb.settingsDAO().setEnviroment(enviroment);
    }

    public void deleteAllSettings(Context context){

        dbLogWriter.info(context, "Delete all Settings");
        mDb.settingsDAO().deleteAllSettings();
    }


}