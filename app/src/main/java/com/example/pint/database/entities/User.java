package com.example.pint.database.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "user")
public class User {

    @PrimaryKey(autoGenerate = true)
    Integer id;

    @ColumnInfo(name = "user_code")
    String user_code;

    @ColumnInfo(name = "name")
    String name;

    @ColumnInfo(name = "vorname")
    String vorname;

    @ColumnInfo(name = "erstanmeldung")
    Integer erstanmeldung;


    public User(){

    }

    @Ignore
    public User(String user_code, String name, String vorname) {
        this.user_code = user_code;
        this.name = name;
        this.vorname = vorname;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUser_code() {
        return user_code;
    }

    public void setUser_code(String user_code) {
        this.user_code = user_code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public Integer getErstanmeldung() {
        return erstanmeldung;
    }

    public void setErstanmeldung(Integer erstanmeldung) {
        this.erstanmeldung = erstanmeldung;
    }

    @Override
    public String toString() {
        return "user{" +
                "id=" + id +
                ", user_code='" + user_code + '\'' +
                ", name='" + name + '\'' +
                ", vorname='" + vorname + '\'' +
                ", erstanmeldung=" + erstanmeldung +
                '}';
    }
}
