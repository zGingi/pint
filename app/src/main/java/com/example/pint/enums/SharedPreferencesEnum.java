package com.example.pint.enums;

public enum SharedPreferencesEnum {

    //Strings
    ACTIVE_USER_ID, ACTIVE_USER, ACTIVE_MODULE, ACTIVE_SESSION, ACTIVE_SOUND_PROFILE,

    //Dates

    //Int

    //Boolean
    APP_FIRST_START,

    //Settings
    IS_DEBUG_ENABLED, ACTIVE_ENVIROMENT, ACTIVE_LANGUAGE,

    //Error Handling
    SIMULATE_UNCAUGHT_EXCEPTION_ENABLED, UNCAUGHT_EXCEPTION_OCCURRED, UNCAUGHT_EXCEPTION_CONTENT

}
