package com.example.pint.enums;


public enum SettingsEnum {

    //@INFO
    //Set default enviroment
    //1 = DEV
    //2 = TEST
    //3 = PROD
    ACTIVE_ENVIROMENT              (1),

    //@INFO
    //Set default language
    //1 = German
    //2 = French
    //3 = Italian
    ACTIVE_LANGUAGE                (1),

    //@INFO
    //Set debug modus
    //0 = Deactivated
    //1 = Activated
    IS_DEBUG_ENABLED               (1),

    //@INFO
    //Set Log retainment in days
    //30 = 30 days before deleted
    //60 = 60 days before deleted
    //90 = 90 days before deleted
    //XX = 30 days before deleted
    LOG_LIFE_IN_DAYS               (60),

    //@INFO
    //Set Archive retainment in days
    //30 = 30 days before deleted
    //60 = 60 days before deleted
    //90 = 90 days before deleted
    //XX = 30 days before deleted
    ARCHIVE_LIFE_IN_DAYS           (60),

    //@INFO
    //Set App Version Number (String)
    APP_VERSION_NUMBER             ("0.0.1"),

    //@INFO
    //Set App Release Date (String)
    APP_RELEASE_DATE               ("04.09.2020");

    private String enumString;
    private int enumInt;

    public String getEnumString() {
        return String.valueOf(enumString);
    }

    SettingsEnum(String enumString) {
        this.enumString = enumString;
    }

    public int getEnumInt() {
        return enumInt;
    }

    SettingsEnum(int enumInt) {
        this.enumInt = enumInt;
    }

}
