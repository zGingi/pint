package com.example.pint;

import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import  com.example.pint.adapter.ListViewAdapter_LogCard;
import  com.example.pint.database.entities.log.DBLog;
import  com.example.pint.database.model.log.DBLogModel;
import  com.example.pint.enums.LogENUM;
import  com.example.pint.util.ErrorHelper;
import  com.example.pint.util.UnCaughtException;
import  com.example.pint.util.ViewUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class LogOverviewActivity extends AppCompatActivity {

    String TAG = LogOverviewActivity.class.getName();
    private static final int REQUEST = 113;

    DBLogModel dbLogModel;

    List<DBLog> all_dbLogs;

    String[] data;
    List<String[]> cardData;
    List<String> allDates;
    ArrayAdapter<String[]> adapterList;

    ListView mlistView;

    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!ViewUtils.isLandscapeEnabled(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        setContentView(R.layout._admin_activity_log_overview);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        Thread.setDefaultUncaughtExceptionHandler(new UnCaughtException(LogOverviewActivity.this));

        dbLogModel = ViewModelProviders.of(LogOverviewActivity.this).get(DBLogModel.class);

        cardData = new ArrayList<>();

        adapterList = new ListViewAdapter_LogCard(LogOverviewActivity.this, cardData);
        mlistView = (ListView) findViewById(R.id.listview_log_overview);

        try {
            new AsyncTask<Void, Void, Void>() {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    dialog = new ProgressDialog(LogOverviewActivity.this);
                    dialog.setMessage("Loading log content.");
                    dialog.show();
                }


                @Override
                protected Void doInBackground(Void... params) {

                    all_dbLogs = dbLogModel.getAllDBLog(false, 0); //allDbLogs used for file System epxort

                    allDates = dbLogModel.getLogDistinctDates();
                    Log.d(TAG, "All Distinct Dates : " + allDates.toString());

                    for (String date : allDates) {
                        data = new String[7];
                        data[0] = date;
                        data[1] = ""+dbLogModel.countLogsForDate(date);
                        data[2] = ""+dbLogModel.countLogsWithTypeForDate(date, LogENUM.ERROR.toString());
                        data[3] = ""+dbLogModel.countLogsWithTypeForDate(date, LogENUM.WARNING.toString());
                        data[4] = ""+dbLogModel.countLogsWithTypeForDate(date, LogENUM.INFO.toString());
                        data[5] = ""+dbLogModel.countLogsWithTypeForDate(date, LogENUM.DEBUG.toString());
                        data[6] = ""+dbLogModel.countLogsWithTypeForDate(date, LogENUM.ARCHIVE.toString());


                        cardData.add(data);
                    }
                    return null;

                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    mlistView.setAdapter(adapterList);
                    adapterList.notifyDataSetChanged();

                    // do UI work here
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
            }.execute().get();
        } catch (InterruptedException | ExecutionException e) {
           new ErrorHelper().showErrorAlert_Exception(LogOverviewActivity.this, e); //e.printStackTrace();
        }

        //@DEBUG
        Log.d(TAG, "Amount of Log Distinct Dates : " + allDates.size());

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


}
