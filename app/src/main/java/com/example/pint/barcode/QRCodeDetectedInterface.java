package com.example.pint.barcode;

/**
 * Created by ima02 on 26.05.2017.
 */

public interface QRCodeDetectedInterface {
    void onQRCodeDetected();
}
